package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws IOException, ServletException {

	    	HttpSession session = request.getSession();
	        List<String> errorMessages = new ArrayList<>();

	        String commentText = request.getParameter("text");
	        String id = request.getParameter("messageId");
	        Integer messageId = Integer.parseInt(id);

	        if (!isValid(commentText, errorMessages)) {
	            session.setAttribute("errorMessages", errorMessages);
	            response.sendRedirect("./");
	            return;
	        }

	        User user = (User) request.getSession().getAttribute("loginUser");

	        Comment comment = new Comment();
	        comment.setUserId(user.getId());
	        comment.setMessageId(messageId);
	        comment.setText(commentText);

	        new CommentService().insert(comment);

	        request.setAttribute("comment", comment);

	        response.sendRedirect("./");

	    }

	    private boolean isValid(String commentText, List<String> errorMessages) {

	        if (StringUtils.isBlank(commentText)) {
	            errorMessages.add("メッセージを入力してください");
	        } else if (140 < commentText.length()) {
	            errorMessages.add("140文字以下で入力してください");
	        }

	        if (errorMessages.size() != 0) {
	            return false;
	        }
	        return true;
	    }



}
