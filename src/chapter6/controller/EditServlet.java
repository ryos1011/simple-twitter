package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		String messageId = request.getParameter("messageId");

		Message message = null;

		if (!StringUtils.isBlank(messageId) && messageId.matches("^[0-9]+$")) {
			int id = Integer.parseInt(messageId);
			message = new MessageService().select(id);
        }

		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
		}

		request.setAttribute("message", message);

		request.getRequestDispatcher("edit.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String editText = request.getParameter("editText");
        String editId = request.getParameter("messageId");

        Message message = new Message();
        message.setText(editText);
        message.setId(Integer.parseInt(editId));

        if (!isValid(editText, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
        }

        new MessageService().update(message);
        response.sendRedirect("./");
	}

	private boolean isValid(String editText, List<String> errorMessages) {

        if (StringUtils.isBlank(editText)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < editText.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
